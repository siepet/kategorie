	<div class="row">
		<div class="col-xs-6 col-sm-4"></div>
		<div class="col-xs-6 col-sm-4">
			<form class="form-horizontal" role="form" action="index.php?action=manusr" method="POST" name="addUser">
			  <div class="form-group">
				<label for="inputLogin3" class="col-sm-2 control-label">Login</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" id="inputLogin3" placeholder="Login" name="login" required>
				</div>
			  </div>
			  <div class="form-group">
				<label for="inputEmail3" class="col-sm-2 control-label">Email</label>
				<div class="col-sm-7">
					<input type="email" class="form-control" id="inputEmail3" placeholder="Email" name="email" required>
				</div>
			  </div>
			  <div class="form-group">
				<label for="inputPassword3" class="col-sm-2 control-label">Hasło</label>
				<div class="col-sm-7">
					<input type="password" class="form-control" id="inputPassword3" placeholder="Hasło" name="password" required>
				</div>
			  </div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<div class="checkbox">
						<label>
							<input type="checkbox" name="access" value="1">Admin?
						</label>
					</div>
				</div>
			</div>
			  <div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-default">Dodaj usera</button>
				</div>
			  </div>
			  <input type="hidden" name="form" value="addUser"/>
			</form>
		</div>
	</div>