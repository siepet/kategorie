<?php
	if(!isset($_SESSION)){
		session_start();
	}
	//print_r($_SESSION);
	/*
	*	TODO:
	*		1. manage users (update)
	*		2. manage categories (update) 
	*		3. print categories as trees(jquery?)
	*		4. ????????
	*
	*/
	ini_set ("display_errors", "1");
	error_reporting(E_ALL);
	require_once 'config.php';
	require_once 'class.messages.php';
	require_once 'db.php';
    $db = new DB();
	$msg = new Messages();
	//$msg->add('s', 'You have successfully visited this page!');
	$showLoginForm = 0;
	$manageUsers = 0;
	$showAddUserForm = 0;
	$changeCatForm = 0;
	$changeUserForm = 0;
	$showDeleteUser = 0;
	$showListUsers = 0;
	$showListCategories = 0;
	$manageCategories = 0;
	$showAddCategoryForm = 0;
	$showIndex = 0;
	$title = "Kategorie - strona główna";
	function __autoload($class_name)
	{
		include $class_name.'.php';
	}
	
	/* database connecting */
	try	{
		$pdo = new PDO($ms.':host='.$hostname.';dbname='.$db_name.';port='.$port, $db_username, $db_password);
		$db->init($pdo);
	} catch(PDOException $e) {
		echo 'Połączenie nie mogło zostać utworzone: '.$e->getMessage();
	}
	
	
	/* site re-routing */
	
	if(isset($_GET['action'])){
		$action = $_GET['action'];
	} else {
		$action = 'index';
	}

	switch ($action){
		case 'login':
			if(!isset($_SESSION['logged'])){
				$showLoginForm = 1;
				$title = "Kategorie - logowanie";
			} else {
				$msg->add('i', "Jesteś aktualnie zalogowany jako ".$_SESSION["username"].".", $manIndex);
			}
			break;
		case 'logout':
			if(isset($_SESSION)){
				$msg->add('i', 'Zostałeś wylogowany!');
				header("Location: ".$manIndex);
				session_destroy();
				exit();
			}
			break;
		case 'manusr';
				if(isset($_SESSION) && $_SESSION['access'] == 1){
					$manageUsers = 1;
					if(isset($_GET['subaction'])) {
						switch($_GET['subaction']){
							case 'adduser':
								$showAddUserForm = 1;
								break;
							case 'deleteuser':
								if($_SESSION['id_user'] == $_GET['id']){
									$msg->add('e', "Nie możesz usunąć sam siebie!", $manUsr);
									//header('Location: http://89.71.154.73:1337/~siepet/index.php?action=manusr');
								} else {
									$usr = $db->getUser($_GET['id']);
									$db->deleteUser($_GET['id']);
									$msg->add('s', "Użytkownik ".$usr['login']." został pomyślnie usunięty!", $manUsr);
								}
								break;
							case 'changeuser':
								$changeUserForm = 1;
								break;
						}
					} else {
						$showListUsers = 1;
					}
					//$showAddUserForm = 1;
					$title = "Kategorie - zarządzaj użytkownikami";
				} else {
					header("Location: ".$manIndex);
				}
			break;
		case 'mancat':
			if(isset($_SESSION['access'])){
				if($_SESSION['access'] == 1){
					$manageCategories = 1;
					if(isset($_GET['subaction'])) {
						switch($_GET['subaction']){
							case 'addcat':
								$showAddCategoryForm = 1;
								break;
							case 'deletecat':
								if($db->hasChildren($_GET['id']) == true){
									$msg->add('e', "Nie możesz głównej kategori, która ma podkategorie!", $manCat);
								} else {
									$cat = $db->getCategory($_GET['id']);
									$db->deleteCategory($_GET['id']);
									$msg->add('s', "Kategoria ".$cat['nazwa']." usunieta pomyślnie!", $manCat);
								}
							
								break;
							case 'changecat':
								$changeCatForm = 1;
								break;
						}
					} else {
						$showListCategories = 1;
					}
					//$showAddCategoryForm = 1;
					$title = "Kategorie - zarządzaj kategoriami";
				} else if($_SESSION['access'] == 0) {
					$manageCategories = 1;
					$showListCategories = 1;
				}
			} else {
				$manageCategories = 1;
				$showListCategories = 1;
			}

			break;
		case 'index':
			$showIndex = 1;
			break;
		
	}
	
	/* form handling */
	
	if(isset($_POST['form'])) {
		$form = $_POST['form'];
	} else {
		$form = 'index';
	}
	switch ($form){
		case 'login':
			login($msg, $db, $_POST['login'], $_POST['password']);
			break;
		case 'addUser':
			if($db->checkForLogin($_POST['login']) && $db->checkForEmail($_POST['email'])){
				$access = 0;
				if(isset($_POST['access'])){
					$access = 1;
				}
				//login mail haslo access
				$db->addUser($_POST['login'], $_POST['email'], $_POST['password'], $access);
				$msg->add('s', "Sukces! Użytkownik ".$_POST['login']." został pomyślnie dodany!");
			} else {
				$msg->add('e', "Błąd! Podany login / e-mail istnieje już w bazie");
				// jest w bazie
			}
			
			break;
		case 'addCategory':
			if($db->checkForCategory($_POST['nazwa'])){
				if($_POST['catSelect'] == "NULL"){
					$db->addCategory($_POST['nazwa']);
				} else {
					$db->addCategory($_POST['nazwa'], $_POST['catSelect']);
				}
				$msg->add('s', "Sukces! Kategoria ".$_POST['nazwa']." została pomyślnie dodana!");
			} else {
				$msg->add('e', 'Błąd! Dana kategoria '.$_POST['nazwa'].' już istnieje!');
			}
			break;
			
		case 'changeUser':
			if(isset($_SESSION['access']) && $_SESSION['access'] == 1){
				$usr = $db->getUser($_POST['idUsr']);
				if(isset($_POST['access'])){
					$db->modifyUser($_POST['idUsr'], $_POST['password'], 1);
				} else {
					$db->modifyUser($_POST['idUsr'], $_POST['password'], 0);
				}
				$msg->add('s',"Użytkownik ".$usr['login']." został zaktualizowany! ", $manUsr);
				
			} else {
				// nie ma dostepu
			}
			break;
			
		case 'changeCategory':
			if(isset($_SESSION['access']) && $_SESSION['access'] == 1){
				$cat1 = $db->getCategory($_POST['idCat']);
				$cat2 = $db->getCategory($_POST['catSelect']);
				if($cat1['id_kategoria'] == $cat2['id_kategoria']) {
					$msg->add('e', "Nie możesz ustawić nadkategorii na samą siebie!", $manCat);
				} else {
					$db->modifyCategory($_POST['idCat'], $_POST['nazwa'], $_POST['catSelect']);
					$msg->add('s', "Kategoria ".$_POST['nazwa']." została pomyślnie uaktualniona!", $manCat);
				}
			} else {
				// nie ma dostepu
			}
			break;
			
		case 'index':
			break;
	}
		
		
	
	function login($msg, $db, $login, $password)
	{
		$data = $db->login($login, $password);
		if($data != false) {
			$_SESSION['id_user'] = $data['id_user'];
			$_SESSION['logged'] = 1;
			$_SESSION['username'] = $data['login'];
			$_SESSION['access'] = $data['access'];
			$msg->add('s', 'Zalogowałeś się jako '.$_SESSION['username'].".");
		} else {
			$msg->add('e', 'Błąd przy logowaniu, spróbuj ponownie!');
		}
	
	}
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf8"/>
	<title><?php echo $title ?></title>
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css"/>
	<link rel="stylesheet" type="text/css" href="assets/css/style.css"/>
	<link rel="shortcut icon" href="http://www.twitch.tv/favicon.ico"/>
	</head>
<body>
	<div class="container">
	<div class="row">
		<div class="col-md-4"></div>
		<div class="col-md-4"><span class="flash"> <?php echo $msg->display(); ?></span></div>
		<div class="col-md-4"></div>
	</div>

		<div class="row">
			<div class="col-md-6">
				<ul class="nav navbar-nav">
					<li><a href="index.php">Home</a></li>
					<?php
						if(!isset($_SESSION['logged'])){
							echo '<li><a href="index.php?action=login">Zaloguj</a></li>';
							echo '<li><a href="index.php?action=mancat">Pokaż kategorie</a></li>';
						} else {
							echo '<li><a href="index.php?action=logout">Wyloguj</a></li>';
							if($_SESSION['access'] == 1){
								echo '<li><a href="index.php?action=manusr">Zarzadzaj użytkownikami</a></li>';
								echo '<li><a href="index.php?action=mancat">Zarządzaj kategoriami</a></li>';
							} else {
								echo "<li><a href=\"index.php?action=mancat\">Pokaż kategorie</a></li>";
							}
						}
					?>
				</ul>
			</div>
		</div>
		<div class="jumbotron">
			<?php
				if ($showLoginForm == 1) {
					echo '<div class="row">';
					echo '<div class="col-xs-6 col-sm-4"></div>';
					echo '<div class="col-xs-6 col-sm-4">';
					echo '<h3>Zaloguj się</h3>';
					echo '</div><div class="col-xs-6 col-sm-4"></div></div>';
					include 'loginForm.html';
				} else if($manageUsers == 1) { 
					echo '<div class="row">';
					echo '<div class="col-xs-6 col-sm-4"></div>';
					echo '<div class="col-xs-6 col-sm-4">';
					if($showAddUserForm == 1) {
						echo '<h3>Dodaj użytkownika</h3>';
						echo '</div><div class="col-xs-6 col-sm-4"></div></div>';
						include 'addUserForm.php';
					} else if($showListUsers == 1){
						echo '<h3>Zarządzaj użytkownikami</h3>';
						echo '</div><div class="col-xs-6 col-sm-4"></div></div>';
						$userList = $db->getUsers();
						include 'showListUsers.php';
					} else if($changeUserForm == 1){
						echo '<h3>Zmień hasło użytkownika</h3>';
						echo '</div><div class="col-xs-6 col-sm-4"></div></div>';
						$user = $db->getUser($_GET['id']);
						include 'changeUserForm.php';
					}
				} else 	if($manageCategories == 1) {
					echo '<div class="row">';
					echo '<div class="col-xs-6 col-sm-4"></div>';
					echo '<div class="col-xs-6 col-sm-4">';
					if($showAddCategoryForm == 1){
						echo '<h3>Dodaj kategorię</h3>';
						echo '</div><div class="col-xs-6 col-sm-4"></div></div>';
						$categories = $db->getAllCategories();
						include 'addCategoryForm.php';
					} else if($showListCategories == 1){
						if(isset($_SESSION['access'])){
							if($_SESSION['access'] == 1){
								echo '<h3>Zarządzaj kategoriami</h3>';
							} else {
								echo '<h3>Pokaż kategorie</h3>';
							}
							echo '</div><div class="col-xs-6 col-sm-4"></div></div>';
							$categories = $db->getAllCategories();
							include 'showListCategories.php';
						} else {
							echo '<h3>Pokaz kategorie</h3>';
							echo '</div><div class="col-xs-6 col-sm-4"></div></div>';
							$categories = $db->getAllCategories();
							include 'showListCategories.php';
						}
					} else if($changeCatForm == 1){
						if(isset($_SESSION['access'])){
							echo '<h3>Zmień kategorię</h3>';
							echo '</div><div class="col-xs-6 col-sm-4"></div></div>';
							$categories = $db->getAllCategories();
							$catEdit = $db->getCategory($_GET['id']);
							include 'changeCategoryForm.php';	
						}
					}
				} else if($showIndex == 1) {
					echo '<h1>Kategorie <small>- strona główna</small></h1>';
					echo '<p>Czołem! Strona ta działa jak powinna :)</p>';
					if(isset($_SESSION['logged']) && $_SESSION['logged'] == 1){
						echo "<p>Aktualnie jesteś zalogowany jako <b>".$_SESSION['username']."</b>, z rolą ";
						if($_SESSION['access'] == 1){
							echo "<b>administratora</b>.";
						} else {
							echo "<b>użytkownika</b>.";
						}
						echo "</p>";
					} else {
						echo '<p><a class="btn btn-lg btn-primary" href="index.php?action=login" role="button">Zaloguj się</a></p>';
					}
				}
			?>
		</div>

		<div class="row">
			<div class="col-md-6 col-offset-10 centered">
				<h6><small>Website made by <a href="mailto: ja@ja.pl">siepet</a>. Copyright &copy; 2014 </small></h6>
			</div>
		</div>
	</div>
	
<script>
setTimeout(function() {
    $('.flash').fadeOut(2000);
}, 3000);
</script>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
</body>
</html>
