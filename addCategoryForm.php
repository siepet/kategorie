	<div class="row">
		<div class="col-xs-6 col-sm-4"></div>
		<div class="col-xs-6 col-sm-4">
			<form class="form-horizontal" role="form" action="index.php?action=mancat" method="POST" name="addCat">
			  <div class="form-group">
				<label for="inputNazwa3" class="col-sm-2 control-label">Nazwa</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" id="inputNazwa3" placeholder="Nazwa" name="nazwa" required>
				</div>
			  </div>
			<div class="form-group">
				<label for="select3" class="col-sm-2 control-label">Nadkat.</label>
			    <div class="col-sm-7">
					<select class="form-control" id="select3" name="catSelect">
						<?php
							echo "<option value=\"NULL\">Brak</option>";
							foreach($categories as $category){
								echo "<option value=\"".$category['id_kategoria']."\">".$category['nazwa']."</option>\n";
							}
						?>
					</select>
				</div>
			</div>
			  <div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-default">Dodaj kategorię</button>
				</div>
			  </div>
			  <input type="hidden" name="form" value="addCategory" />
			</form>
		</div>
	</div>