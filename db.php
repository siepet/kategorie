<?php

class DB
{
	private $pdo;
	
	public function init($pdo)
	{
		$this->pdo = $pdo;
		$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}
	
	/*
	*	Returns true if user doesn't exist in the database.
	*/

	public function checkForLogin($user)
	{
		try {
			$query = "SELECT login FROM users WHERE login='".$user."';";
			$result = $this->pdo->query($query);
			if($result->fetch(PDO::FETCH_NUM > 0)) {
				return false;
			} else {
				return true;
			}
		} catch(PDOException $e) {
			echo $e->getMessage();
		}
	}
	
	public function checkForEmail($email)
	{
		$query = "SELECT login FROM users WHERE email='".$email."';";
		$result = $this->pdo->query($query);
		if($result->fetch(PDO::FETCH_NUM > 0)) {
			return false;
		} else {
			return true;
		}
	}
	
	public function checkForCategory($category)
	{
		$query = "SELECT * FROM kategoria WHERE nazwa='".$category."';";
		$result = $this->pdo->query($query);
		if($result->fetch(PDO::FETCH_NUM > 0)) {
			return false;
		} else {
			return true;
		}
	}
	
	
	public function addCategory($catName, $id_nad = null)
	{
		if($this->checkForCategory($catName)){
			try {
				if($id_nad == null){
					$this->pdo->exec("INSERT INTO kategoria(nazwa) VALUES('".$catName."');");
					return true;
				} else {
					$this->pdo->exec("INSERT INTO kategoria(nazwa, id_nadkategoria) VALUES('".$catName."', '".$id_nad."');");
					return true;
				}
			} catch(PDOException $e) {
				return false;
				//echo $e->getMessage();
			}
		}
	}
	
	public function addUser($login, $email, $password, $access = 0)
	{
		$password = md5($password);
		if($this->checkForLogin($login)){
			try {
				$this->pdo->exec("INSERT INTO users(login, email, password, access) VALUES('".$login."', '".$email."', '".$password."', ".$access.");");
			} catch (PDOException $e) {
				echo $e->GetMessage();
			}
		}
	}
		
	public function login($login, $password)
	{
		$password = md5($password);
		$query = "SELECT * FROM users WHERE login='".$login."' AND password='".$password."';";
		try {
			$result = $this->pdo->query($query);
			$data = $this->pdo->query($query);
			
			if($result->fetch(PDO::FETCH_NUM > 0)) {
				$data = $data->fetch();
				return array("id_user" => $data['id'], "login" => $data['login'], "access" => $data['access']);			
			} else {
				return false;
			}
			
		} catch (PDOException $e){
			echo $e->getMessage();
		}


	}
	
	public function deleteUser($id)
	{
		try {
			$this->pdo->exec("DELETE FROM users WHERE id='".$id."';");
		} catch (PDOException $e) {
			echo $e->GetMessage();
		}
	}
	
	public function deleteCategory($id)
	{
		try {
			$this->pdo->exec("DELETE FROM kategoria WHERE id_kategoria=".$id.";");
		} catch (PDOException $e) {
			echo $e->GetMessage();
		}
	}
	
	public function modifyUser($id, $newPassword, $access = -1)
	{
		try {
			$newPassword = md5($newPassword);
			if($access == -1){
				$result = $this->pdo->exec("UPDATE users SET password='".$newPassword."' WHERE id=".$id.";");
				//echo "UPDATE users SET password='".$newPassword."' WHERE id=".$id.";";
			} else {
				$result = $this->pdo->exec("UPDATE users SET password='".$newPassword."', access='".$access."' WHERE id=".$id.";");
				//echo "UPDATE users SET password='".$newPassword."', access='".$access."' WHERE id=".$id.";";
			}
		} catch (PDOException $e) {
			echo $e->GetMessage();
		}
	}
	
	public function modifyCategory($id, $newName, $newParent)
	{
		try {
			if($newParent == "NULL"){
				$result = $this->pdo->exec("UPDATE kategoria SET nazwa='".$newName."', id_nadkategoria=NULL WHERE id_kategoria='".$id."'");
			} else {
				$result = $this->pdo->exec("UPDATE kategoria SET nazwa='".$newName."', id_nadkategoria='".$newParent."' WHERE id_kategoria='".$id."'");
			}
		} catch (PDOException $e) {
			echo $e->GetMessage();
		}
	}
	
	public function getUser($id)
	{
		try {
			$result = $this->pdo->query("SELECT * FROM users WHERE id=".$id.";");
			return $result->fetch();
		} catch (PDOException $e) {
			echo $e->GetMessage();
		}
	}
	
	public function getUsers($access = -1)
	{
		try {
			if($access == -1){
				$result = $this->pdo->query("SELECT * from users;");
			} else {
				$result = $this->pdo->query("SELECT * FROM users WHERE access=".$access.";");
			}
			return $result->fetchAll();
		} catch (PDOException $e) {
			echo $e->GetMessage();
		}
	}
	
	public function getAllCategories()
	{
		try {
			$result = $this->pdo->query("SELECT * from kategoria;");
			return $result->fetchAll();
		} catch (PDOException $e) {
			echo $e->GetMessage();
		}
	}
	
	public function getAllMainCategories()
	{
		try {
			$result = $this->pdo->query("SELECT * from kategoria WHERE id_nadkategoria IS NULL;");
			return $result->fetchAll();
		} catch (PDOException $e) {
			echo $e->GetMessage();
		}
	}
	
	public function hasChildren($id)
	{
		try {
			$result = $this->pdo->query("SELECT * from kategoria WHERE id_nadkategoria='".$id."';");
			if($result->fetch(PDO::FETCH_NUM > 0)) {
				return true;
			} else {
				return false;
			}
		} catch (PDOException $e) {
			echo $e->GetMessage();
		}	
	}
	
	
	public function getCategory($id)
	{
		try {
			$result = $this->pdo->query("SELECT * from kategoria WHERE id_kategoria='".$id."';");
			return $result->fetch();
		} catch (PDOException $e) {
			echo $e->GetMessage();
		}
	}

}
