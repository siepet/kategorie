<div class="row">
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>ID</th>
				<th>Login</th>
				<th>E-mail</th>
				<th>Typ konta</th>
				<th>Akcja</th>
		</thead>
		<tbody>
		<?php
			foreach($userList as $user){
			echo "<tr>\n";
			echo "<td>".$user['id']."</td>\n";
			echo "<td>".$user['login']."</td>\n";
			echo "<td>".$user['email']."</td>\n";
			echo "<td>";
			if($user['access'] == 1){
				echo "administrator";
			} else {
				echo "użytkownik";
			}
			echo "</td>\n";
			echo "<td><a href=\"index.php?action=manusr&subaction=changeuser&id=".$user['id']."\">Zmień</a> | <a href=\"index.php?action=manusr&subaction=deleteuser&id=".$user['id']."\">Usuń</a></td>\n";
			echo "</tr>\n";
			}		
		?>
		</tbody>	
	</table>
	<a class="btn btn-lg btn-primary" href="index.php?action=manusr&subaction=adduser" role="button">Dodaj użytkownika</a>
</div>