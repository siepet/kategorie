	<div class="row">
		<div class="col-xs-6 col-sm-4"></div>
		<div class="col-xs-6 col-sm-4">
			<form class="form-horizontal" role="form" action="index.php?action=manusr" method="POST" name="addUser">
			  <div class="form-group">
				<label for="inputLogin3" class="col-sm-2 control-label">Login</label>
				<div class="col-sm-7">
					<div class="col-sm-7">
						<div class="form-control-static readonly"><?php echo $user['login']; ?></div>
					</div>					
				</div>
			  </div>
			  <div class="form-group">
				<label for="inputPassword3" class="col-sm-2 control-label">Hasło</label>
				<div class="col-sm-7">
					<input type="password" class="form-control" id="inputPassword3" placeholder="Hasło" name="password" required>
				</div>
			  </div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<div class="checkbox">
						<label>
							<?php
								if($user['access'] == 1){
									echo '<input type="checkbox" name="access" value="0" checked>Admin?';
									echo "\n";
								} else {
									echo '<input type="checkbox" name="access" value="1">Admin?';
									echo "\n";
								}
							?>							
						</label>
					</div>
				</div>
			</div>
			  <div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-default">Zmień</button>
				</div>
			  </div>
			  <input type="hidden" name="idUsr" value="<?php echo $user['id']; ?>"/>
			  <input type="hidden" name="form" value="changeUser"/>
			</form>
		</div>
	</div>