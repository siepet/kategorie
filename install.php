<?php
	$mss = array("mysql", "psql", "oracle", "mssql");
	$configFile = "config.php";
//    echo $configFile;
	if(file_exists($configFile)){
		$installed = 1;
        //echo "istnieje";
       // print_r($_POST);
	} else {
        //echo "nie istnieje";
        if(isset($_POST['submit'])){
            //echo "wyslano forma";
            //print_r($_POST);
			$fileHandle = fopen($configFile, "w");
			fwrite($fileHandle, "<?php \n");
			fwrite($fileHandle, "\t".'$ms = "'.$_POST['msSelect'].'";'."\n");
			fwrite($fileHandle, "\t".'$hostname = "'.$_POST['hostname'].'";'."\n");
			fwrite($fileHandle, "\t".'$port = '.$_POST['port'].';'."\n");
			fwrite($fileHandle, "\t".'$db_username = "'.$_POST['dbUsername'].'";'."\n");
			fwrite($fileHandle, "\t".'$db_password = "'.$_POST['dbPassword'].'";'."\n");
			fwrite($fileHandle, "\t".'$db_name = "'.$_POST['dbDb'].'";'."\n");
			fwrite($fileHandle, "\t".'$manCat = "'.'http://'.$_SERVER['HTTP_HOST']."/index.php?action=mancat".'";'."\n");
			fwrite($fileHandle, "\t".'$manUsr = "'.'http://'.$_SERVER['HTTP_HOST']."/index.php?action=manusr".'";'."\n");
			fwrite($fileHandle, "\t".'$manIndex = "'.'http://'.$_SERVER['HTTP_HOST']."/index.php".'";'."\n");
			fwrite($fileHandle, "?>");
			fclose($fileHandle);
			initialize_database($configFile, $_POST['adminLogin'], $_POST['adminPassword'], $_POST['adminEmail']);
			header('Location: install.php');
		} else {
            //echo "nie wyslano forma";
            //echo print_r($_POST);
        }
	}
	function initialize_database($configFile, $adminLogin, $adminPassword, $adminEmail)
	{	
		include $configFile;
		try {
			$pdo = new PDO($ms.':host='.$hostname.';dbname='.$db_name.';port='.$port, $db_username, $db_password);
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			/* creating table kategoria */
			$pdo->query("CREATE TABLE kategoria(id_kategoria INTEGER PRIMARY KEY AUTO_INCREMENT, nazwa VARCHAR(50) NOT NULL, id_nadkategoria INTEGER references kategoria(id_kategoria));");
			
			/* creating table users */
			$pdo->query("CREATE TABLE users(id INTEGER PRIMARY KEY AUTO_INCREMENT, login VARCHAR(20) NOT NULL, email VARCHAR(30) NOT NULL, password VARCHAR(50) NOT NULL, access INTEGER NOT NULL, date TIMESTAMP);");
			
			/* adding default admin to the database */
			$adminPassword = md5($adminPassword);
			$pdo->query("INSERT INTO users(login, email, password, access) VALUES('".$adminLogin."', '".$adminEmail."', '".$adminPassword."', 1);");
            
            /* adding sample users */
            $pdo->query("INSERT INTO users(login, email, password, access) VALUES('ewa', 'ewa@o2.pl', 'ewa123', 0);");
            $pdo->query("INSERT INTO users(login, email, password, access) VALUES('jan', 'jan@o2.pl', 'jan123', 0);");

			/* adding sample kategorie */
			$pdo->query("INSERT INTO kategoria(nazwa, id_nadkategoria) VALUES('Ssaki', NULL);");
			$pdo->query("INSERT INTO kategoria(nazwa, id_nadkategoria) VALUES('Prototheria', 1);");
			$pdo->query("INSERT INTO kategoria(nazwa, id_nadkategoria) VALUES('Monotremata', 2);");
			$pdo->query("INSERT INTO kategoria(nazwa, id_nadkategoria) VALUES('Theria', 1);");

		} catch(PDOException $e) {
			echo $e->getMessage();
		}
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf8"/>
	<title>Kategorie - instalacja</title>
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css"/>
	<link rel="stylesheet" type="text/css" href="assets/css/style.css"/>
	<link rel="shortcut icon" href="http://www.twitch.tv/favicon.ico"/>
	</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<ul class="nav navbar-nav">
					<li><a href="index.php">Home</a></li>
					<li><a href="http://siepet.pl">Author</a></li>
				</ul>
			</div>
		</div>
		<div class="jumbotron">
			<?php
			if($installed != 1){
			echo '
			<h1>Kategorie - instalacja</h1>
			<p>Aby korzystać z pełni dzałającej aplikacji, wpierw ją skonfiguruj:</p>
			<form class="form-horizontal" role="form" action="install.php" method="POST" name="install">
				<div class="form-group">
					<label for="hostname" class="col-sm-2 control-label" title="Adres hosta na którym jest postawiony serwer">hostname:</label>
						<div class="col-sm-7">
							<input type="text" class="form-control" id="hostname" value="localhost" name="hostname" required>
						</div>
				</div>
				<div class="form-group">
					<label for="port" class="col-sm-2 control-label" title="Port hosta na którym jest postawiony serwer">port:</label>
						<div class="col-sm-7">
							<input type="text" class="form-control" id="port" value="3306" name="port" required>
						</div>
				</div>
				<div class="form-group">
					<label for="dbUsername" class="col-sm-2 control-label" title="Nazwa użytkownika bazy danych">db_username:</label>
						<div class="col-sm-7">
							<input type="text" class="form-control" id="dbUsername" value="root" name="dbUsername" required>
						</div>
				</div>
				<div class="form-group">
					<label for="dbPassword" class="col-sm-2 control-label" title="Hasło użytkownika bazy danych">db_password:</label>
						<div class="col-sm-7">
							<input type="password" class="form-control" id="dbPassword" value="" name="dbPassword">
						</div>
				</div>
				<div class="form-group">
					<label for="dbDb" class="col-sm-2 control-label" title="Nazwa bazy danych">db_name:</label>
						<div class="col-sm-7">
							<input type="text" class="form-control" id="dbDb" value="" name="dbDb" required>
						</div>
				</div>
				<div class="form-group">
					<label for="select3" class="col-sm-2 control-label" title="System zarzadzania bazy danych">m_system</label>
						<div class="col-sm-7">
							<select class="form-control" id="select3" name="msSelect">
								';
									foreach($mss as $system){
										echo "<option>".$system."</option>";
									}
								echo '
							</select>
						</div>
				</div>
				<div class="form-group">
					<label for="adminLogin" class="col-sm-2 control-label" title="Login administratora aplikacji">admin_login:</label>
						<div class="col-sm-7">
							<input type="text" class="form-control" id="adminLogin" placeholder="login" name="adminLogin" required>
						</div>
				</div>
				<div class="form-group">
					<label for="adminPassword" class="col-sm-2 control-label" title="Hasło administratora aplikacji">admin_password:</label>
						<div class="col-sm-7">
							<input type="password" class="form-control" id="adminPassword" placeholder="password" name="adminPassword" required>
						</div>
				</div>
				<div class="form-group">
					<label for="adminEmail" class="col-sm-2 control-label" title="Adres e-mail administratora aplikacji">admin_e-mail:</label>
						<div class="col-sm-7">
							<input type="email" class="form-control" id="adminEmail" placeholder="e-mail" name="adminEmail" required>
						</div>
				</div>
				<input type="hidden" name="submit" value="true"/>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-lg btn-primary">Zainstaluj</button>
					</div>
				</div>
			</form>
			'; 
			} else {
				echo '
				<h1>Kategorie <small>- aplikacja jest już zainstalowana!<small></h1>
				<p>Już możesz się cieszyć poprawnie zainstalowaną aplikacją!</p>
				<p><a class="btn btn-lg btn-success" href="index.php" role="button">Kliknij aby przejść</a></p>
				';
			}
			?>
			
		</div>
		<div class="row">
			<div class="col-md-6 col-offset-10 centered">
				<h6><small>Website made by <a href="mailto: ja@ja.pl">siepet</a>. Copyright &copy; 2014 </small></h6>
			</div>
		</div>
	</div>
</body>
</html>
