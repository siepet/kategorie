<div class="row">
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>ID</th>
				<th>Nazwa</th>
				<th>Nadkategoria</th>
				<?php
				if(isset($_SESSION['access']) && $_SESSION['access'] == 1){
					echo "<th>Akcja</th>";
				} 
				?>
			</tr>
		</thead>
		<tbody>
		<?php
			foreach($categories as $category){
			echo "<tr>\n";
			echo "<td>".$category['id_kategoria']."</td>\n";
			echo "<td>".$category['nazwa']."</td>\n";
			$nadKatName = $db->getCategory($category['id_nadkategoria']);
			if($category['id_nadkategoria'] == null){
				echo "<td>Kategoria główna</td>";
			} else {
				echo "<td>".$nadKatName['nazwa']."</td>\n";
			}
			if(isset($_SESSION['access']) && $_SESSION['access'] == 1){
				echo "<td><a href=\"index.php?action=mancat&subaction=changecat&id=".$category['id_kategoria']."\">Zmień</a> ";
				echo "| <a href=\"index.php?action=mancat&subaction=deletecat&id=".$category['id_kategoria']."\">Usuń</a></td>\n";
			}
			echo "</tr>\n";
			}		
		?>
		</tbody>
	
	</table>
	<?php
		if(isset($_SESSION['access']) && $_SESSION['access'] == 1){
			echo '<a class="btn btn-lg btn-primary" href="index.php?action=mancat&subaction=addcat" role="button">Dodaj kategorię</a>';
		} 
	?>	
</div>